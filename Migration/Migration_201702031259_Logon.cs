﻿using FluentMigrator;

namespace Migrations
{
    [Migration(201702031259)]
    public class Migration_201702031259_Logon  : Migration
    {
        public override void Up()
        {
            var sql = string.Format(@"
                                if not exists (select 1 from sys.server_principals s where s.name = '{0}')
                                begin
	                                CREATE LOGIN [{0}] WITH PASSWORD = '{1}';
                                end
                                ", Constants.UserName, Constants.Password);
            Execute.Sql(sql);
        }

        public override void Down()
        {
            var sql = string.Format(@"
                                if exists (select 1 from sys.server_principals s where s.name = '{0}')
                                begin
	                                DROP LOGIN [{0}];
                                end
                                ", Constants.UserName);
            Execute.Sql(sql);
        }
    }
}