﻿using System.Data.SqlClient;
using FluentMigrator;

namespace Migrations
{
    [Migration(201702031321)]
    public class Migration_201702031321_DbUsers : Migration
    {
        public override void Up()
        {
            Execute.WithConnection((c, t) =>
            {
                foreach (var tableName in Constants.TableNames)
                {
                    using (var con = new SqlConnection(c.ConnectionString))
                    {
                        con.Open();
                        var cmd = con.CreateCommand();
                        cmd.CommandText = string.Format(@"
                                            USE [{0}];
                                            CREATE USER [{1}] FOR LOGIN [{1}] WITH DEFAULT_SCHEMA=[dbo];
                                            EXEC sp_addrolemember 'db_owner', '{1}';
                                            USE [{2}];
                                         ", tableName, Constants.UserName, cmd.Connection.Database);
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            });
        }

        private void SetDbBackToMain()
        {
            Execute.WithConnection((c, t) =>
            {
                using (var con = new SqlConnection(c.ConnectionString))
                {
                    con.Open();
                    var cmd = con.CreateCommand();
                    var dbname = cmd.Connection.Database;
                    cmd.CommandText = string.Format("USE [{0}];", dbname);
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            });
        }
        public override void Down()
        {
            Execute.WithConnection((c, t) =>
            {
                foreach (var tableName in Constants.TableNames)
                {
                    using (var con = new SqlConnection(c.ConnectionString))
                    {
                        con.Open();
                        var cmd = con.CreateCommand();

                        var sql = string.Format(@"
                                            USE [{0}];
                                            if exists( SELECT 1 FROM sys.database_principals where name = '{0}')
                                            begin
                                                DROP USER  [{1}];
                                            end
                                            USE [{2}];
                                         ", tableName, Constants.UserName, cmd.Connection.Database);
                        cmd.CommandText = sql;
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            });
        }
    }
}