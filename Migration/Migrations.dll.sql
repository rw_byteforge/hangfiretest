/* Using Connection Debug from Configuration file C:\Users\Richard\Projects\Personal\HangfireTest\Migration\App.config */
/* VersionMigration migrating ================================================ */

/* Beginning Transaction */
/* CreateTable VersionInfo */
CREATE TABLE [dbo].[VersionInfo] ([Version] BIGINT NOT NULL)

/* Committing Transaction */
/* VersionMigration migrated */

/* VersionUniqueMigration migrating ========================================== */

/* Beginning Transaction */
/* CreateIndex VersionInfo (Version) */
CREATE UNIQUE CLUSTERED INDEX [UC_Version] ON [dbo].[VersionInfo] ([Version] ASC)

/* AlterTable VersionInfo */
/* No SQL statement executed. */

/* CreateColumn VersionInfo AppliedOn DateTime */
ALTER TABLE [dbo].[VersionInfo] ADD [AppliedOn] DATETIME

/* Committing Transaction */
/* VersionUniqueMigration migrated */

/* VersionDescriptionMigration migrating ===================================== */

/* Beginning Transaction */
/* AlterTable VersionInfo */
/* No SQL statement executed. */

/* CreateColumn VersionInfo Description String */
ALTER TABLE [dbo].[VersionInfo] ADD [Description] NVARCHAR(1024)

/* Committing Transaction */
/* VersionDescriptionMigration migrated */

/* 201702031259: Migration_201702031259_Logon migrating ====================== */

/* Beginning Transaction */
/* ExecuteSqlStatement 
                                if not exists (select 1 from sys.server_principals s where s.name = 'HangfireTestUser')
                                begin
	                                CREATE LOGIN [HangfireTestUser] WITH PASSWORD = 'B71F1197-24C9-414E-A0C8-67E810427D83';
                                end
                                 */

                                if not exists (select 1 from sys.server_principals s where s.name = 'HangfireTestUser')
                                begin
	                                CREATE LOGIN [HangfireTestUser] WITH PASSWORD = 'B71F1197-24C9-414E-A0C8-67E810427D83';
                                end
                                

INSERT INTO [dbo].[VersionInfo] ([Version], [AppliedOn], [Description]) VALUES (201702031259, '2017-02-03T20:32:33', 'Migration_201702031259_Logon')
/* Committing Transaction */
/* 201702031259: Migration_201702031259_Logon migrated */

/* 201702031305: Migration_201702031305_Databases migrating ================== */

/* Beginning Transaction */
/* PerformDBOperation  */
/* Performing DB Operation */

INSERT INTO [dbo].[VersionInfo] ([Version], [AppliedOn], [Description]) VALUES (201702031305, '2017-02-03T20:32:33', 'Migration_201702031305_Databases')
/* Committing Transaction */
/* 201702031305: Migration_201702031305_Databases migrated */

/* 201702031321: Migration_201702031321_DbUsers migrating ==================== */

/* Beginning Transaction */
/* PerformDBOperation  */
/* Performing DB Operation */

INSERT INTO [dbo].[VersionInfo] ([Version], [AppliedOn], [Description]) VALUES (201702031321, '2017-02-03T20:32:33', 'Migration_201702031321_DbUsers')
/* Committing Transaction */
/* 201702031321: Migration_201702031321_DbUsers migrated */

/* Task completed. */
