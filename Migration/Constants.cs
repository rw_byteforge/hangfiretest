﻿using System.Collections.Generic;

namespace Migrations
{
    public static class Constants
    {
        public static string UserName => "HangfireTestUser";
        public static string Password => "B71F1197-24C9-414E-A0C8-67E810427D83";
        public static List<string> TableNames = new List<string>
        {
            "HangfireMain",
            "HangfireClientOne",
            "HangfireClientTwo",
            "HangfireClientThree"
        };
    }
}