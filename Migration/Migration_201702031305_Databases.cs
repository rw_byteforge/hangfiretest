﻿using System.Data.SqlClient;
using FluentMigrator;

namespace Migrations
{
    [Migration(201702031305)]
    public class Migration_201702031305_Databases : Migration
    {
        public override void Up()
        {
            Execute.WithConnection((c, t) =>
            {
                foreach (var dbName in Constants.TableNames)
                {
                    using (var con = new SqlConnection(c.ConnectionString))
                    {
                        con.Open();
                        var cmd = con.CreateCommand();
                        cmd.CommandText = string.Format(@"CREATE DATABASE [{0}];", dbName);
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            });
        }

        public override void Down()
        {
            Execute.WithConnection((c, t) =>
            {
                foreach (var dbName in Constants.TableNames)
                {
                    using (var con = new SqlConnection(c.ConnectionString))
                    {
                        con.Open();
                        var cmd = con.CreateCommand();
                        cmd.CommandText = string.Format(@"DROP DATABASE [{0}];", dbName);
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            });
        }
    }
}