﻿using System;
using System.Configuration;
using Common;
using Hangfire;
using Hangfire.SqlServer;
using HangfireClientThree.Jobs;
using Ninject.Modules;

namespace HangfireClientThree
{
    public class JobModule : NinjectModule
    {
        public static string AppName => ConfigurationManager.AppSettings["AppName"] ?? "HangfireClientThree";
        public static string JobQueue => ConfigurationManager.AppSettings["Jobs_BasicQueue"] ?? "hfc_three_jobs";
        public static string JobCron => ConfigurationManager.AppSettings["Job_HfcThreeJobOne"] ?? "*/15 * * * *"; //Every 15 mins

        public override void Load()
        {
        }
        public static void ScheduleJobs()
        {
            ScheduleRecurringJob<HfcThreeJobOne>(JobQueue);
        }
        private static void ScheduleRecurringJob<T>(string queue) where T : IRecurringJob
        {
            var jobName = typeof(T).Name;
            var cron = JobCron;
            cron = (string.IsNullOrWhiteSpace(cron) ? "#" : cron);
            if (cron.StartsWith("#"))
                return;

            RecurringJob.AddOrUpdate<T>($"{AppName} {jobName}", x => x.Execute(queue), cron, queue: queue, timeZone: TimeZoneInfo.Local);
        }
    }
}