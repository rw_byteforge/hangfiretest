﻿using System;
using System.ComponentModel;
using Common;
using Hangfire;
using HangfireClientThree.Filters;
using Ninject.Extensions.Logging;

namespace HangfireClientThree.Jobs
{
    public class HfcThreeJobOne : IRecurringJob
    {
        private readonly ILogger _logger;

        public HfcThreeJobOne(ILogger logger)
        {
            _logger = logger;
        }

        [DisplayName("{0} HangfireClientThree")]
        [UseQueueFromFirstParameterFilter]
        [DisableConcurrentExecution(timeoutInSeconds: 120)]
        [DisableMultipleQueuedItemsFilter]
        public void Execute(string queue)
        {
            _logger.Debug("HangfireClientThree.Jobs", $"HangfireClientThree.Jobs: {DateTime.Now}");
        }
    }
}