﻿using Hangfire.Common;
using Hangfire.States;

namespace HangfireClientThree.Filters
{
    public class UseQueueFromFirstParameterFilterAttribute : JobFilterAttribute, IElectStateFilter
    {
        public void OnStateElection(ElectStateContext context)
        {
            var enqueuedState = context.CandidateState as EnqueuedState;
            if (enqueuedState != null)
            {
                enqueuedState.Queue = (string)context.BackgroundJob.Job.Args[0];
            }
        }
    }
}