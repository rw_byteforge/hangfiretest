﻿namespace Common
{
    public interface IRecurringJob
    {
        void Execute(string queue);
    }
}
