﻿using Hangfire;
using Hangfire.SqlServer;
using HangfireClientThree;
using HangfireClientTwo;
using Microsoft.Owin;
using Ninject;
using Owin;

[assembly: OwinStartup(typeof(HangfireStartup))]
namespace HangfireClientTwo
{
    public class HangfireStartup
    {
        public void Configuration(IAppBuilder app)
        {
            var kernel = new StandardKernel();
            kernel.Load<JobModule>();

            var sqlopts = new SqlServerStorageOptions
            {
                PrepareSchemaIfNecessary = true
            };
            GlobalConfiguration.Configuration.UseNinjectActivator(kernel);
            GlobalConfiguration.Configuration
                .UseSqlServerStorage("hangfire", sqlopts);
            app.UseHangfireDashboard("/jobs");
            app.UseHangfireServer(new BackgroundJobServerOptions
            {
                Queues = new[] {JobModule.JobQueue}
            });
            JobModule.ScheduleJobs();
        }
    }
}