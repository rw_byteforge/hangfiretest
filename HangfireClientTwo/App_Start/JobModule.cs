﻿using System;
using System.Configuration;
using Common;
using Hangfire;
using HangfireClientThree.Jobs;
using Ninject.Modules;

namespace HangfireClientTwo
{
    public class JobModule : NinjectModule
    {
        public static string AppName => ConfigurationManager.AppSettings["AppName"] ?? "HangfireClientTwo";
        public static string JobQueue => ConfigurationManager.AppSettings["Jobs_BasicQueue"] ?? "hfc_two_jobs";
        public static string JobCron => ConfigurationManager.AppSettings["Job_HfcTwoJobOne"] ?? "*/20 * * * *"; //Every 20 mins

        public override void Load()
        {
        }
        public static void ScheduleJobs()
        {
            ScheduleRecurringJob<HfcTwoJobOne>(JobQueue);
        }
        private static void ScheduleRecurringJob<T>(string queue) where T : IRecurringJob
        {
            var jobName = typeof(T).Name;
            var cron = JobCron;
            cron = (string.IsNullOrWhiteSpace(cron) ? "#" : cron);
            if (cron.StartsWith("#"))
                return;

            RecurringJob.AddOrUpdate<T>($"{AppName} {jobName}", x => x.Execute(queue), cron, queue: queue, timeZone: TimeZoneInfo.Local);
        }
    }
}