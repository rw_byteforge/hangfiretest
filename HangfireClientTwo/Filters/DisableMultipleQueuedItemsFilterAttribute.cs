using System;
using System.Collections.Generic;
using System.Globalization;
using Hangfire.Client;
using Hangfire.Common;
using Hangfire.Server;
using Hangfire.Storage;

namespace HangfireClientTwo.Filters
{
    public class DisableMultipleQueuedItemsFilterAttribute : JobFilterAttribute, IClientFilter, IServerFilter
    {
        private static readonly TimeSpan LockTimeout = TimeSpan.FromSeconds(5);
        private static readonly TimeSpan FingerprintTimeout = TimeSpan.FromHours(1);

        public void OnCreating(CreatingContext filterContext)
        {
            if (!AddFingerprintIfNotExists(filterContext.Connection, filterContext.Job))
            {
                filterContext.Canceled = true;
            }
        }

        public void OnPerformed(PerformedContext filterContext)
        {
            RemoveFingerprint(filterContext.Connection, filterContext.BackgroundJob.Job);
        }

        private static bool AddFingerprintIfNotExists(IStorageConnection connection, Job job)
        {
            using (connection.AcquireDistributedLock(GetFingerprintLockKey(job), LockTimeout))
            {
                var fingerprint = connection.GetAllEntriesFromHash(GetFingerprintKey(job));

                DateTimeOffset timestamp;

                if (fingerprint != null &&
                    fingerprint.ContainsKey("Timestamp") &&
                    DateTimeOffset.TryParse(fingerprint["Timestamp"], null, DateTimeStyles.RoundtripKind, out timestamp) &&
                    DateTimeOffset.UtcNow <= timestamp.Add(FingerprintTimeout))
                {
                    // Actual fingerprint found, returning.
                    return false;
                }

                // Fingerprint does not exist, it is invalid (no `Timestamp` key),
                // or it is not actual (timeout expired).
                connection.SetRangeInHash(GetFingerprintKey(job), new Dictionary<string, string>
                {
                    { "Timestamp", DateTimeOffset.UtcNow.ToString("o") }
                });

                return true;
            }
        }

        private static void RemoveFingerprint(IStorageConnection connection, Job job)
        {
            using (connection.AcquireDistributedLock(GetFingerprintLockKey(job), LockTimeout))
            using (var transaction = connection.CreateWriteTransaction())
            {
                transaction.RemoveHash(GetFingerprintKey(job));
                transaction.Commit();
            }
        }

        private static string GetFingerprintLockKey(Job job)
        {
            return $"{GetFingerprintKey(job)}:lock";
        }

        private static string GetFingerprintKey(Job job)
        {
            return $"fingerprint:{GetFingerprint(job)}";
        }

        private static string GetFingerprint(Job job)
        {
            var parameters = string.Empty;
            if (job.Args != null)
            {
                parameters = string.Join(".", job.Args);
            }
            if (job.Type == null || job.Method == null)
            {
                return string.Empty;
            }
            var fingerprint = $"{job.Type.FullName}.{job.Method.Name}.{parameters}";

            return fingerprint;
        }

        void IClientFilter.OnCreated(CreatedContext filterContext)
        {
            //unused
        }

        void IServerFilter.OnPerforming(PerformingContext filterContext)
        {
            //unused
        }
    }
}