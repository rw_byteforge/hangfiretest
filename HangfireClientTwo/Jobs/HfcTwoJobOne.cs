﻿using System;
using System.ComponentModel;
using Common;
using Hangfire;
using HangfireClientTwo.Filters;
using Ninject.Extensions.Logging;

namespace HangfireClientThree.Jobs
{
    public class HfcTwoJobOne : IRecurringJob
    {
        private readonly ILogger _logger;

        public HfcTwoJobOne(ILogger logger)
        {
            _logger = logger;
        }

        [DisplayName("{0} HangfireClientTwo")]
        [UseQueueFromFirstParameterFilter]
        [DisableConcurrentExecution(timeoutInSeconds: 120)]
        [DisableMultipleQueuedItemsFilter]
        public void Execute(string queue)
        {
            _logger.Debug("HangfireClientTwo.Jobs", $"HangfireClientTwo.Jobs: {DateTime.Now}");
        }
    }
}