﻿using Hangfire;
using Hangfire.SqlServer;
using HangfireTest;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(HangfireStartup))]
namespace HangfireTest
{
    public class HangfireStartup
    {
        public void Configuration(IAppBuilder app)
        {
            var sqlopts = new SqlServerStorageOptions
            {
                PrepareSchemaIfNecessary = true
            };
            GlobalConfiguration.Configuration
                .UseSqlServerStorage("hangfire", sqlopts);
            app.UseHangfireDashboard("/jobs");
            app.UseHangfireServer();
        }
    }
}