# Hangfire Test
One of our suppositions has been that Hangfire just sucks when it comes to how it handles multi-tenancy.
I think it's more that we're using it wrong.

Currently we're trying to use a single Hangfire database for all instances.
If what I've read is correct, it's not really intended to be used that way, which gives rise to what we're seeing in the logs & in the admin interface.

In this project there's a single migration that should build out all of the databases that you'll need to test. You'll want to change the connection string in Migration/App.Config and build that project in Debug mode.

In order to migrate up, simply open a powershell/cmd window to the Migration directory & invoke the Migrate.bat file. If you want to migrate down, which will remove all created databases & users, modify the Migrate.bat file & replace /t:Migrate with /t:MigrateRollbackAll, save the file, then re-invoke.

The project has two run modes, CentralDb & DbPerApp.
In CentralDb, Hangfire is run as we currently have it, each app touching the same db.
It has the exact same problems you'd see in our current deployments.

In DbPerApp, we switch to each app having their own Hangfire db.
The only shared component here is a common user/login. However, the admin screens appear to be much cleaner and more aligned with what everyone expects it to look like.
