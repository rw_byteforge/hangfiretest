﻿using System;
using System.ComponentModel;
using Common;
using Hangfire;
using HangfireClientOne.Filters;
using Ninject.Extensions.Logging;

namespace HangfireClientOne.Jobs
{
    public class HfcOneJobOne : IRecurringJob
    {
        private readonly ILogger _logger;

        public HfcOneJobOne(ILogger logger)
        {
            _logger = logger;
        }

        [DisplayName("{0} HangfireClientOne")]
        [UseQueueFromFirstParameterFilter]
        [DisableConcurrentExecution(timeoutInSeconds: 120)]
        [DisableMultipleQueuedItemsFilter]
        public void Execute(string queue)
        {
            _logger.Debug("HangfireClientOne.Jobs", $"HangfireClientOne.Jobs: {DateTime.Now}");
        }
    }
}