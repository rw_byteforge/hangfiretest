﻿using System;
using System.Configuration;
using Common;
using Hangfire;
using Hangfire.SqlServer;
using HangfireClientOne.Jobs;
using Ninject.Modules;

namespace HangfireClientOne
{
    public class JobModule : NinjectModule
    {
        public static string AppName => ConfigurationManager.AppSettings["AppName"] ?? "HangfireClientOne";
        public static string JobQueue => ConfigurationManager.AppSettings["Jobs_BasicQueue"] ?? "hfc_one_jobs";
        public static string JobCron => ConfigurationManager.AppSettings["Job_HfcOneJobOne"] ?? "*/10 * * * *"; //Every 10 mins

        public override void Load()
        {
        }
        public static void ScheduleJobs()
        {
            ScheduleRecurringJob<HfcOneJobOne>(JobQueue);
        }
        private static void ScheduleRecurringJob<T>(string queue) where T : IRecurringJob
        {
            var jobName = typeof(T).Name;
            var cron = JobCron;
            cron = (string.IsNullOrWhiteSpace(cron) ? "#" : cron);
            if (cron.StartsWith("#"))
                return;

            RecurringJob.AddOrUpdate<T>($"{AppName} {jobName}", x => x.Execute(queue), cron, queue: queue, timeZone: TimeZoneInfo.Local);
        }
    }
}